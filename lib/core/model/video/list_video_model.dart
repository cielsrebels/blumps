// To parse this JSON data, do
//
//     final videoModel = videoModelFromMap(jsonString);

import 'dart:convert';

List<VideoModel> videoModelFromMap(String str) =>
    List<VideoModel>.from(json.decode(str).map((x) => VideoModel.fromMap(x)));

String videoModelToMap(List<VideoModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class VideoModel {
  VideoModel({
    this.id,
    this.userId,
    this.userName,
    this.video,
    this.videoCaption,
    this.videoDesc,
    this.isDelete,
    this.rememberToken,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  int? userId;
  String? userName;
  String? video;
  String? videoCaption;
  String? videoDesc;
  dynamic isDelete;
  dynamic rememberToken;
  String? createdAt;
  String? updatedAt;

  factory VideoModel.fromMap(Map<String, dynamic> json) => VideoModel(
        id: json["id"],
        userId: json["user_id"],
        userName: json["user_name"],
        video: json["video"],
        videoCaption: json["video_caption"],
        videoDesc: json["video_desc"],
        isDelete: json["is_delete"],
        rememberToken: json["remember_token"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "user_id": userId,
        "user_name": userName,
        "video": video,
        "video_caption": videoCaption,
        "video_desc": videoDesc,
        "is_delete": isDelete,
        "remember_token": rememberToken,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}
