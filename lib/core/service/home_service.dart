import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:blumps/core/model/video/list_video_model.dart';
import 'package:http/io_client.dart';

class HomeService extends ChangeNotifier {
  // var endpoint = "http://192.168.0.118/ft_admin/public/api/";
  var endpoint = "https://blumps.info/public/api/";
  var timeout = 20;

  Future<List<VideoModel>> getList() async {
    try {
      final ioc = HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      final client = IOClient(ioc);
      final response =
          await client.get(Uri.parse(endpoint + "listmobile"), headers: {
        "Accept": "application/json",
      }).timeout(Duration(seconds: HomeService().timeout));
      Iterable data = jsonDecode(response.body);
      List<VideoModel> listData =
          data.map((e) => VideoModel.fromMap(e)).toList();
      notifyListeners();
      return listData;
    } on DioError catch (e) {
      // ignore: avoid_print
      print(e.toString());
      return [];
    }
  }
}
