import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:blumps/core/helper/dio_exception.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService extends ChangeNotifier {
  Response? response;
  Dio dio = Dio();
  var endpoint = "https://blumps.info/apis/api/";
  var timeout = 20;
  BuildContext? context;

  Future<bool> checkLogin(
      String strEmail, String strPassword, BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      response = await dio
          .post(endpoint + "login",
              options: Options(headers: {
                "Accept": "application/json",
              }),
              data: FormData.fromMap(
                  {'email': strEmail, 'password': strPassword}))
          .timeout(Duration(seconds: timeout));
      if (response?.data['isSucces'] == true) {
        prefs.setBool('is_login', true);
        prefs.setString('token', response?.data['token']);
        prefs.setString(
            'courier_id', response!.data['data_user']['id'].toString());
        prefs.setString(
            'email', response!.data['data_user']['email'].toString());
        prefs.setString('password', strPassword);
        prefs.setString('message', "Login berhasil");
        return true;
      } else {
        Fluttertoast.showToast(
            msg: "Email / Password Salah",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return false;
      }
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();
      Fluttertoast.showToast(
          msg: errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
  }
}
