import 'package:flutter/material.dart';
import 'package:blumps/core/model/video/list_video_model.dart';
import 'dart:async';
import 'package:blumps/core/service/home_service.dart';

class HomeProvider extends ChangeNotifier {
  String linkserver = "https://base-app.com/ft_admin/public";
  List<VideoModel> listVideo = [];

  Future<List<VideoModel>> getListVideo() async {
    listVideo = await HomeService().getList();
    notifyListeners();
    return listVideo;
  }

  // Future<List<VideoModel>> getListVideo() async {
  //   var data = [
  //     {
  //       "id": 1,
  //       "user_id": 5,
  //       "user_name": "Ryuki",
  //       "tumbnail":
  //           "https://cdn.idntimes.com/content-images/community/2019/05/iron-man-header-f99ad485400903a231ee96da6ca7c5b3_600x400.jpg",
  //       "video":
  //           "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
  //       "video_caption": "Iron Man",
  //       "video_desc": "Description",
  //       "is_delete": "",
  //       "created_at": "2021-10-01",
  //       "updated_at": "2021-10-01"
  //     },
  //     {
  //       "id": 2,
  //       "user_id": 5,
  //       "user_name": "Ryuki",
  //       "tumbnail":
  //           "https://img.beritasatu.com/cache/beritasatu/620x350-2/251453796279.jpg",
  //       "video": "https://media.w3.org/2010/05/sintel/trailer.mp4",
  //       "video_caption": "Tony Stark",
  //       "video_desc": "Description",
  //       "is_delete": "",
  //       "created_at": "2021-10-01",
  //       "updated_at": "2021-10-01"
  //     }
  //   ];
  //   listVideo = data.map((e) => VideoModel.fromMap(e)).toList();
  //   // ignore: avoid_print
  //   print(listVideo.length);
  //   return listVideo;
  // }
}
