import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider extends ChangeNotifier {
  // final fb = FacebookLogin();

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  Future<bool> signInWithGoogle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final GoogleSignInAccount? googleSignInAccount =
          await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount!.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      var login = await _auth.signInWithCredential(credential);
      if (login.user != null) {
        prefs.setBool('is_login', true);
        prefs.setString('email', login.user!.email.toString());
        prefs.setString('name', login.user!.displayName.toString());
        prefs.setString('photoProfile', login.user!.photoURL.toString());
        prefs.setString('loginWith', 'google');
        return true;
      }
      return false;
    } on FirebaseAuthException catch (e) {
      prefs.setString('message', e.toString());
      throw e.toString();
    }
  }

  Future<void> signOutFromGoogle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await _googleSignIn.signOut();
    await _auth.signOut();
    prefs.clear();
    notifyListeners();
  }

  // Future signInWithFacebook() async {
  //   final res = await fb.logIn(permissions: [
  //     FacebookPermission.publicProfile,
  //     FacebookPermission.email,
  //   ]);

  //   switch (res.status) {
  //     case FacebookLoginStatus.success:

  //       // Get profile data
  //       final profile = await fb.getUserProfile();
  //       print('Hello, ${profile!.name.toString()}! You ID: ${profile.userId}');

  //       // Get user profile image url
  //       final imageUrl = await fb.getProfileImageUrl(width: 100);
  //       print('Your profile image: $imageUrl');

  //       // Get email (since we request email permission)
  //       final email = await fb.getUserEmail();
  //       // But user can decline permission
  //       if (email != null) print('And your email is $email');

  //       break;
  //     case FacebookLoginStatus.cancel:
  //       // User cancel log in
  //       break;
  //     case FacebookLoginStatus.error:
  //       // Log in failed
  //       print('Error while log in: ${res.error}');
  //       break;
  //   }

  // Future<void> gooleSignout() async {
  //   await _auth.signOut().then((onValue) {
  //     fb.logOut();
  //   });
  // }
}
