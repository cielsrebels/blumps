import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:blumps/core/viewmodel/auth/auth_provider.dart';
import 'package:blumps/ui/homescreen/homescreen.dart';
import 'package:blumps/ui/login/login.dart';
import 'package:blumps/ui/onboarding/onboarding.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  // ignore: unused_field
  bool visible = true;

  late AnimationController animationController;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {});
    startSplashScreen();
  }

  // @override
  // dispose() {
  //   super.dispose();
  //   Provider.of<AuthProvider>(context).dispose();
  // }

  startSplashScreen() async {
    animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 3));
    animation =
        CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    // ignore: avoid_returning_null_for_void, unnecessary_this
    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      visible = !visible;
    });
    var duration = const Duration(seconds: 3);
    return Timer(duration, () async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      // prefs.clear();
      if (prefs.getBool('is_login') == true) {
        var login = await Provider.of<AuthProvider>(context, listen: false)
            .signInWithGoogle();
        if (login == true) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => const HomeScreen()),
          );
        }
      } else {
        if (prefs.getBool('onboarding') == true) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => const LoginScreen(),
          ));
        } else {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => const OnboardingScreen(),
          ));
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.black, // navigation bar color
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarIconBrightness: Brightness.light, // status bar color
    ));
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(fit: StackFit.expand, children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              padding: const EdgeInsets.all(30),
              child: Center(
                child: Image.asset(
                  "assets/blump-png.png",
                  width: animation.value * 300,
                  height: animation.value * 300,
                ),
              ),
            ),
          ],
        ),
      ]),
    );
  }
}
