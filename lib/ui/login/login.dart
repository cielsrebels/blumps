import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:blumps/core/viewmodel/auth/auth_provider.dart';
import 'package:blumps/ui/homescreen/homescreen.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: HomeBody(),
    );
  }
}

class HomeBody extends StatelessWidget {
  const HomeBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authProv = Provider.of<AuthProvider>(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              padding: const EdgeInsets.only(bottom: 60),
              child: Center(
                child: Image.asset(
                  "assets/blump-png.png",
                  height: 100,
                ),
              )),
          const Text(
            'Sign in to watch video with firends',
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
          const SizedBox(height: 20),
          SizedBox(
            width: MediaQuery.of(context).size.width / 1.6,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                    onTap: () async {
                      // await authProv.signInWithFacebook();
                      Fluttertoast.showToast(
                          msg: "Unevailable now",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0);
                    },
                    child: buttonSignin('assets/facebook.png')),
                InkWell(
                    onTap: () {
                      Fluttertoast.showToast(
                          msg: "Unevailable now",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0);
                    },
                    child: buttonSignin('assets/twitter.png')),
                InkWell(
                    onTap: () async {
                      bool login = await authProv.signInWithGoogle();
                      if (login == true) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const HomeScreen()),
                        );
                      }
                    },
                    child: buttonSignin('assets/google.png')),
              ],
            ),
          )
        ],
      )),
    );
  }

  Widget buttonSignin(String image) {
    return Container(
      height: 50,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(60)),
      child: Image.asset(
        image,
        fit: BoxFit.fill,
      ),
    );
  }
}
