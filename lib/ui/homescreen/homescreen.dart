import 'package:flutter/material.dart';
import 'package:blumps/core/viewmodel/homescreen/home_provider.dart';
import 'package:blumps/ui/homescreen/movie/movie_player.dart';
import 'package:blumps/ui/homescreen/movie/video_widget.dart';
import 'package:blumps/ui/homescreen/widget/profile.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final homeProv = Provider.of<HomeProvider>(context);
    if (homeProv.listVideo.isEmpty) {
      homeProv.getListVideo();
    }
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.black,
        title: const Text(
          'Blumps',
          style: TextStyle(color: Colors.white, fontSize: 18),
        ),
        actions: [
          IconButton(
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProfileApp(
                          imgUrl: prefs.getString('photoProfile').toString(),
                          name: prefs.getString('name').toString(),
                          email: prefs.getString('email').toString(),
                        )));
              },
              icon: const Icon(
                Icons.person,
                color: Colors.white54,
                size: 30,
              ))
        ],
      ),
      body: homeProv.listVideo.isEmpty
          ? const Center(child: CircularProgressIndicator())
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView.builder(
                  itemCount: homeProv.listVideo.length,
                  physics: const BouncingScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, i) {
                    return moviesItem(
                        int.parse(homeProv.listVideo[i].id.toString()),
                        homeProv.listVideo[i].videoCaption.toString(),
                        homeProv.listVideo[i].videoDesc.toString(),
                        "${homeProv.linkserver}/${homeProv.listVideo[i].video.toString()}");
                  }),
            ),
    );
  }

  Widget moviesItem(int listid, String title, String desc, String movie) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) =>
                MoviePlayer(caption: title, linkmovie: movie)));
      },
      child: Container(
        margin: const EdgeInsets.all(6),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.grey)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 80,
              width: MediaQuery.of(context).size.width / 3.5,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                ),
                child: VideoWidget(
                  play: false,
                  url: movie,
                ),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    title,
                    style: const TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(
                    height: 2,
                  ),
                  Text(
                    desc,
                    style: const TextStyle(color: Colors.white54),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
