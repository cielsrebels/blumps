import 'package:blumps/core/viewmodel/homescreen/home_provider.dart';
import 'package:blumps/ui/splash/splashscreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'core/viewmodel/auth/auth_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => AuthProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => HomeProvider(),
        ),
        // ChangeNotifierProvider(
        //   create: (context) => PageProvider(),
        // ),
      ],
      child: MaterialApp(
        title: "Base App",
        theme: ThemeData(
            fontFamily: 'Proxima-Regular',
            primaryColor: Colors.red,
            colorScheme:
                ColorScheme.fromSwatch().copyWith(secondary: Colors.red)),
        debugShowCheckedModeBanner: false,
        home: const SplashScreen(),
      ),
    );
  }
}
